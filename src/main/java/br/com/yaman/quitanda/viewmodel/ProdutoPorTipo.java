package br.com.yaman.quitanda.viewmodel;

public class ProdutoPorTipo {
	private String nome;
	private Long quantidade;
	
	public ProdutoPorTipo(String nome, Long quantidade) {
		super();
		this.nome = nome;
		this.quantidade = quantidade;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Long getQuantidade() {
		return quantidade;
	}
	
}
