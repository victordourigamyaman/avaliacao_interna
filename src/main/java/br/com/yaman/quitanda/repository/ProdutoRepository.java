package br.com.yaman.quitanda.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.yaman.quitanda.dao.entity.Produto;
import br.com.yaman.quitanda.viewmodel.ProdutoPorTipo;

@Repository
@Transactional
public interface ProdutoRepository extends JpaCustomRepository<Produto> {
	@Query("SELECT new br.com.yaman.quitanda.viewmodel.ProdutoPorTipo(tp.nome, count(p.tipoProduto) as count) from Produto p INNER JOIN p.tipoProduto tp group by p.tipoProduto, tp.nome")
	public List<ProdutoPorTipo> listarPorGrupo();
}
