package br.com.yaman.quitanda.service;

import java.util.List;

import br.com.yaman.quitanda.dao.entity.Produto;
import br.com.yaman.quitanda.viewmodel.ProdutoPorTipo;

public interface ProdutoService extends GenericCrudService<Produto> {
	public List<ProdutoPorTipo> listarPorGrupo();
}
